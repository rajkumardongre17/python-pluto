import pint


ureg = pint.UnitRegistry()
# Add PLUTO-specific units as defined in section B.2
ureg.define('Ohm = ohm')
ureg.define('r = revolution')
ureg.define('AU = au')
# TODO: Remove the following as soon as pint PR #811 is released
# https://github.com/hgrecco/pint/pull/811
ureg.define('d = day')  # waiting for pint #811 to be released
ureg.define('neper = [] = Np')  # waiting for pint #811 to be released
ureg.define('h = hr')  # Redefine to work around h being the Planck constant
# see https://github.com/hgrecco/pint/issues/719
ureg.autoconvert_offset_to_baseunit = True  # workaround for #22
