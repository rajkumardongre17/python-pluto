from pint import _DEFAULT_REGISTRY
ureg = _DEFAULT_REGISTRY


def to_string(value, eng_unit=None):
    if eng_unit is not None:
        value.ito(eng_unit)
    return str(value)


def to_boolean(value):
    if value == 'TRUE':
        return True
    elif value == 'FALSE':
        return False


def to_hex(value):
    return hex(value)


def to_integer(value):
    return int(value, 0)


def to_real(value):
    return float(value)


def capitalize(value):
    return value.capitalize()


def get_from(value, st_index, end_index):
    return value[st_index-1: end_index]


def insert_in(sub, value, index):
    return value[:index-1] + sub + value[index-1:]


def is_contained_in(value_1, value_2):
    return value_1 in value_2


def length_of(value):
    return len(value)


def lower_case(value):
    return value.lower()


def omit_from(value, st_index, end_index):
    return value[:st_index-1] + value[end_index:]


def position_of(value_1, value_2):
    return value_2.find(value_1) + 1


def upper_case(value):
    return value.upper()
