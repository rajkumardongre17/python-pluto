"""The Monitoring and Control Model (MCM) is based on the Space System Model
as defined in ECSS-E-ST-70-31C. It is used to model the system that is to
be controlled and monitored."""


class SystemElement:

    def __init__(self, name, description=None, type=None):
        self.name = name
        self.description = description
        self.type = type
        self.child_element = {}
        # self.redundant_element = []
        self.reporting_data = {}
        self.activity = {}
        self.events = {}

    def __getattr__(self, key):
        if key in self.child_element:
            return self.child_element[key]
        elif key in self.reporting_data:
            return self.reporting_data[key]
        elif key in self.activity:
            return self.activity[key]
        elif key in self.event:
            return self.event[key]
        else:
            raise ValueError

    def add_child_element(self, system_element):
        self.child_element[system_element.name] = system_element

    def add_activity(self, activity):
        self.activity[activity.name] = activity

    def add_reporting_data(self, reporting_data):
        self.reporting_data[reporting_data.name] = reporting_data

    def add_event(self, event):
        self.event[event.name] = event


class ReportingData:

    def __init__(self, name, description=None, type=None):
        self.name = name
        self.description = description
        self.type = type
        # self.domains = []
        # self.domain_validity = []

    def get_value(self):
        raise NotImplementedError


class Activity:

    def __init__(self, name, description=None, type=None):
        self.name = name
        self.description = description
        # self.type = type
        # self.domains = []
        # self.criticality = None

    def run(self):
        raise NotImplementedError


class Event:

    def __init__(self, name, system_element, description=None, type=None):
        self.name = name
        self.system_element = system_element
        self.description = description
        # self.type = type
        # self.domains = []
        # self.severity = None
        # self.domain_specific_activities = []
